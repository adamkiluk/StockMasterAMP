﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using StockMasterAMP.CommonUI;
using StockMasterAMP.Server.Hubs.HangfireJobs;
using StockMasterAMP.Server.Managers;
using StockMasterAMP.Shared.DataModels;
using StockMasterAMP.Shared.Dto.Account;
using StockMasterAMP.WebScraper.Bankier;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StockMasterAMP.Server.Tests.Account
{
    class AccountMangerTests
    {
        private AccountManager _accountManager;

        private Mock<UserManager<ApplicationUser>> _userManager;
        private Mock<SignInManager<ApplicationUser>> _signInManager;
        private Mock<ILogger<AccountManager>> _logger;
        private Mock<RoleManager<IdentityRole<Guid>>> _roleManager;
        private Mock<IEmailManager> _emailManager;
        private Mock<IBankierManager> _bankierManager;
        private Mock<IUserProfileStore> _userProfileStore;
        private Mock<IConfiguration> _configuration;
        private ApplicationUser _mockedUser = new ApplicationUser();

        [SetUp]
        public void SetUp()
        {
            // Break out some of this to a base class that can be shared by tests. This would suck to set up in all test blocks. 
            var userStore = new Mock<IUserStore<ApplicationUser>>();
            var roleStore = new Mock<IRoleStore<IdentityRole<Guid>>>();

            var roles = new List<IRoleValidator<IdentityRole<Guid>>>();
            roles.Add(new RoleValidator<IdentityRole<Guid>>());

            var contextAccessor = new Mock<IHttpContextAccessor>();
            var userPrincipalFactory = new Mock<IUserClaimsPrincipalFactory<ApplicationUser>>();

            var mockedUser = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                UserName = "UnitTestUser",
                Email = "UnitTestUser@mail.com",
            };

            _mockedUser = mockedUser;
            _userManager = new Mock<UserManager<ApplicationUser>>(userStore.Object, null, null, null, null, null, null, null, null);
            _signInManager = new Mock<SignInManager<ApplicationUser>>(_userManager.Object, contextAccessor.Object, userPrincipalFactory.Object, null, null, null, null);

            _logger = new Mock<ILogger<AccountManager>>();
            _roleManager = new Mock<RoleManager<IdentityRole<Guid>>>(roleStore.Object, roles, new UpperInvariantLookupNormalizer(), new IdentityErrorDescriber(), null);
            _emailManager = new Mock<IEmailManager>();
            _bankierManager = new Mock<IBankierManager>();
            _userProfileStore = new Mock<IUserProfileStore>();
            _configuration = new Mock<IConfiguration>();

            _accountManager = new AccountManager(_userManager.Object,
                _signInManager.Object,
                _logger.Object,
                _roleManager.Object,
                _emailManager.Object,
                _userProfileStore.Object,
                _configuration.Object);

            _signInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .ReturnsAsync(SignInResult.Success);

            _userManager.Setup(x => x.DeleteAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(IdentityResult.Success);
            _userManager.Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            _userManager.Setup(x => x.UpdateAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(IdentityResult.Success);
            _userManager.Setup(x => x.ChangeEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            _userManager.Setup(x => x.FindByNameAsync(It.IsAny<string>()))
                 .ReturnsAsync(mockedUser);

            userStore.Setup(x => x.CreateAsync(mockedUser, CancellationToken.None))
                .Returns(Task.FromResult(IdentityResult.Success));

            userStore.Setup(x => x.FindByNameAsync(mockedUser.UserName, CancellationToken.None))
                .Returns(Task.FromResult(mockedUser));
        }

        [Test]
        public void SetupWorked()
        {
            Assert.Pass();
        }

        [Test]
        public async Task ConfirmEmail_WithInvaildParameters_Returns404()
        {
            // Arange 
            var confirmEmailDto = new ConfirmEmailDto();
            confirmEmailDto.Token = null;
            confirmEmailDto.UserId = null;

            // Act
            var response = await _accountManager
                .ConfirmEmail(confirmEmailDto);

            // Assert
            Assert.That(response.StatusCode, Is.EqualTo(404));
        }

        [Test]
        public async Task RegisterUser_ShouldCreateNewUser()
        {
            //Arrange
            var registerDto = new Mock<RegisterDto>();
            registerDto.Object.Email = "UnitTestUser@email.com";
            registerDto.Object.UserName = "UnitTestUser";
            registerDto.Object.Password = "UnitTestUserPassword";
            registerDto.Object.PasswordConfirm = "UnitTestUserPassword";

            //Act
            var response = await _accountManager.Create(registerDto.Object);

            //Assert
            Assert.That(response.Message, Is.EqualTo("Created New User"));
            Assert.That(response.StatusCode, Is.EqualTo(200));
        }

        [Test]
        public async Task Delete_ShouldDeleteUser()
        {
            //Arrange
            //specific setup case should be isolated
            var user = await _userManager.Object.FindByNameAsync("UnitTestUser");
            _userManager.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                 .ReturnsAsync(_mockedUser);
            //Act
            var response = await _accountManager.Delete(user.Id.ToString());

            //Assert
            Assert.That(response.Message, Is.EqualTo("User Deletion Successful"));
            Assert.That(response.StatusCode, Is.EqualTo(200));
        }

        [Test]
        public async Task Delete_WithWrongData_ShouldThrow()
        {
            //Arrange
            //overwriting SetUp method to return null user in FindByIdAsync call
            _userManager.Setup(x => x.FindByIdAsync(It.IsRegex(
                @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                + "@"
                + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$")))
                 .ReturnsAsync((ApplicationUser)null);

            var user = await _userManager.Object.FindByNameAsync("UnitTestUser");

            //Act
            var response = await _accountManager.Delete(user.Email);

            //Assert
            Assert.That(response.Message, Is.EqualTo("User does not exist"));
            Assert.That(response.StatusCode, Is.EqualTo(404));
        }

        [Test]
        public async Task TEST()
        {
            //await new DiscordJobs().ProceedEvery5minsSubscriptionNotifications();
            //new SendDiscordMessage().BuldAndSendNewsSummaryMesseage().GetAwaiter().GetResult();

            //temp test methods
            //BankierScraper scrapper = new BankierScraper();
            //scrapper.GetNews();
            //BankierSymbolsLoader loader = new BankierSymbolsLoader();
            //loader.GetSymbols();
        }
    }
}
