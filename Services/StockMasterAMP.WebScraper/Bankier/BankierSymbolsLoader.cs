﻿using HtmlAgilityPack;
using StockMasterAMP.Shared.Dto.Bankier;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace StockMasterAMP.WebScraper.Bankier
{
    public class BankierSymbolsLoader
    {
        public static List<BankierSymbolDto> GetSymbols()
        {
            List<BankierSymbolDto> BankierSymbolsDto = new List<BankierSymbolDto>();

            Dictionary<string, BankierStockTypeDto> bankierUri = new Dictionary<string, BankierStockTypeDto>()
            {
                { "akcje", BankierStockTypeDto.GPW },
                { "new-connect", BankierStockTypeDto.NewConnect },
            };

            foreach (KeyValuePair<string, BankierStockTypeDto> uri in bankierUri)
            {
                try
                {
                    using (HttpClient client = new HttpClient())
                    {
                        using (HttpResponseMessage response = client.GetAsync(@"https://www.bankier.pl/gielda/notowania/" + uri.Key).Result)
                        {
                            using (HttpContent content = response.Content)
                            {
                                string result = content.ReadAsStringAsync().Result;
                                HtmlDocument document = new HtmlDocument();
                                document.LoadHtml(result);
                                var nodes = document.DocumentNode.SelectNodes("//tbody//tr//td[@class='colWalor textNowrap']//a[1]");

                                foreach (var node in nodes)
                                {
                                    if (node.FirstChild.Name == "#text")
                                    {
                                        BankierSymbolsDto.Add(new BankierSymbolDto()
                                        {
                                            Href = node.Attributes.Where(x => x.Name == "href").FirstOrDefault().Value,
                                            Name = node.InnerText,
                                            Title = node.Attributes.Where(x => x.Name == "title").FirstOrDefault().Value,
                                            StockType = uri.Value,
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    //not implemented as <apiresponse>
                }
            }


            return BankierSymbolsDto;
        }
    }
}
