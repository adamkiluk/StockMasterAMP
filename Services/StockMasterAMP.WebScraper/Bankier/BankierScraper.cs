﻿using HtmlAgilityPack;
using StockMasterAMP.Shared.Dto.Bankier;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace StockMasterAMP.WebScraper.Bankier
{
    public class BankierScraper
    {
        public static List<BankierNewsDto> GetNews(BankierSymbolDto symbolDto)
        {
            List<BankierNewsDto> bankierNewsDto = new List<BankierNewsDto>();

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage response = client.GetAsync(@"https://www.bankier.pl" + symbolDto.Href).Result)
                    {
                        using (HttpContent content = response.Content)
                        {
                            string result = content.ReadAsStringAsync().Result;
                            HtmlDocument document = new HtmlDocument();
                            document.LoadHtml(result);
                            var nodes = document.DocumentNode.SelectNodes("//div[@class='boxContent boxList']//ul");

                            //news Section
                            var newsSection = nodes[0].ChildNodes;
                            HtmlNodeCollection elements = new HtmlNodeCollection(null);
                            Regex regex = new Regex(@"[^-]+(?=\.html$)");

                            foreach(var item in newsSection)
                            {
                                if(item.Name != "#text")
                                {
                                    var bankierId = regex.Match(item.FirstChild.Attributes[0].Value).ToString();

                                    bankierNewsDto.Add(new BankierNewsDto()
                                    {
                                        BankierId = Convert.ToInt32(bankierId),
                                        BankierDateTime = Convert.ToDateTime(item.FirstChild.FirstChild.InnerText),
                                        Href = item.FirstChild.Attributes[0].Value,
                                        Title = item.FirstChild.LastChild.InnerText,
                                        NewsType = BankierNewsTypeDto.News,
                                    });
                                }
                            }

                            //epsiInfo section
                            var epsiTypeSection = nodes[1].ChildNodes;

                            foreach (var item in epsiTypeSection)
                            {
                                if (item.Name != "#text")
                                {
                                    var bankierId = regex.Match(item.FirstChild.Attributes[0].Value).ToString();

                                    bankierNewsDto.Add(new BankierNewsDto()
                                    {
                                        BankierId = Convert.ToInt32(bankierId),
                                        BankierDateTime = Convert.ToDateTime(item.FirstChild.FirstChild.InnerText),
                                        Href = item.FirstChild.Attributes[0].Value,
                                        Title = item.FirstChild.LastChild.InnerText,
                                        NewsType = BankierNewsTypeDto.Epsi,
                                    });
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                //not implemented as <apiresponse>
            }

            return bankierNewsDto;
        }
    }
}
