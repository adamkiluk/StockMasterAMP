﻿using StockMasterAMP.Shared.Dto.Bankier;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Discord
{
    public interface IDiscordWebhook
    {
        Task SendMessage(List<BankierNewsDto> dto);
    }
}
