﻿using Discord;
using Discord.Webhook;
using StockMasterAMP.Shared.Dto.Bankier;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockMasterAMP.Discord
{
    public class DiscordWebhook : IDiscordWebhook
    {
        //ep
        private readonly static DiscordWebhookClient client = new DiscordWebhookClient(801539006675943444, "WF35SqO_aBMkA-z2CMSMzZp1tjvOFfl60rTNk6Lt3s_dAvlFPotJfrRuJjaUV_VyDzYQ");
        
        //private
        //private readonly static DiscordWebhookClient client = new DiscordWebhookClient(802616237666074634, "JedM2wx8Fp-UthFiXQmCVtFGYLWhh8VhhVDLeP7ks-2LWdKO-hVM8aeK-hphkJIVpx2V");
        
        public async Task SendMessage(List<BankierNewsDto> dto)
        {
            List<string> subscribedStockNames = dto.Select(x => x.Name).Distinct().ToList();
            if(dto.Count != 0)
            {
                await SendTitle("@here Every 5 minutes notification generator");
                foreach (string name in subscribedStockNames)
                {
                    List<BankierNewsDto> filteredBankierNews = dto
                        .Where(x => x.Name == name)
                        .OrderByDescending(x => x.BankierDateTime)
                        .Take(5)
                        .ToList();
                    await PrepareandSendBankierNewsDiscordWebhook(filteredBankierNews, name);
                }
            }
        }

        private static async Task SendTitle(string title)
        {
            await client.SendMessageAsync(text: title);
        }

        private static async Task PrepareandSendBankierNewsDiscordWebhook(List<BankierNewsDto> dto, string name)
        {
            foreach(BankierNewsDto item in dto)
            {
                EmbedBuilder builder = new EmbedBuilder()
                {
                    Title = item.Name,
                    Url = $"https://www.bankier.pl{item.Href}",
                    Description = item.Title,
                    Timestamp = item.BankierDateTime
                };
                //await client.SendMessageAsync(text: $"https://www.bankier.pl{item.Href}");
                await client.SendMessageAsync(embeds: new[] { builder.Build() });
            }
        }
    }
}
