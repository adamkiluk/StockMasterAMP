## Architecture

-   Based on articles :

1. https://trailheadtechnology.com/entity-framework-core-2-1-automate-all-that-boring-boiler-plate/
2. https://salslab.com/a/safely-logging-api-requests-and-responses-in-asp-net-core/
3. https://vmsdurano.com/asp-net-core-and-web-api-a-custom-wrapper-for-managing-exceptions-and-consistent-responses/
4. https://stormpath.com/blog/tutorial-policy-based-authorization-asp-net-core
5. https://www.c-sharpcorner.com/UploadFile/1492b1/crud-operations-using-automapper-in-mvc-application/
6. https://devblogs.microsoft.com/aspnet/upcoming-samesite-cookie-changes-in-asp-net-and-asp-net-core/
7. https://web.dev/samesite-cookies-explained/
8. https://chrissainty.com/a-detailed-look-at-data-binding-in-blazor/
9. https://remibou.github.io/Using-the-Blazor-form-validation/
10. https://www.thereformedprogrammer.net/handling-entity-framework-core-database-migrations-in-production-part-1/

    If you implement something based on external source, please include article here

## Goals

-   for WEB SCRAPING we will prob use :

1. https://anglesharp.github.io/ -> free
2. https://html-agility-pack.net/ -> free, best option imo

-   CHARTS :

1. https://github.com/mariusmuntean/ChartJs.Blazor

## Done

-   Docker support only visual studio

## Docker support

-   Build docker-compose solution
-   Open DockerWindows Dashboard or open http://localhost:53414/ to use

## License

-   MIT License

### Dependencies

   - .NET Core SDK 3.1
   > https://dotnet.microsoft.com/download/dotnet-core/3.1 
   - Visual Studio 2019 (v16.6)
   > https://visualstudio.microsoft.com/pl/
   - SQL EXPRESS with LocalDB (newest version preferable)
   > https://www.microsoft.com/pl-pl/sql-server/sql-server-downloads
   - SSMS (newest version preferable)
   > https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15
   - Docker (19.03.12) && Docker-compose (1.26.2)
   > https://docs.docker.com/get-docker/

### How to run - Visual Studio

1. Install the **dependencies**.
2. Check the DB status withing **SSMS**. (Check the connection string if other DB picked)
3. Get project files locally (**git clone**).
4. Open the solution in **Visual Studio**, select the **StockMasterAMP.Server** and press **F5**.
5. To view the API using Swagger UI, Run the solution and go to:
> http://localhost:53414/swagger/index.html

### How to run - with docker

1. Get **docker** && **docker-compose**.
2. Get project files locally:
```
git clone <ssh> <dir>
```
Run in Terminal/PowerShell from the `src` dir:
```
docker-compose up -f docker-compose.yml -p 'stockamp' -d --remove-orphans 
```
View the API using Swagger UI at:
> http://localhost:53414/swagger/index.html

## Contributing

Please star, watch and fork! We'd greatly appreciate any contribution you make.

#### 0.2.1
-   Added Docker support for Linux. thanks to [@adamkiluk](https://gitlab.com/adamkiluk)
-   Edited README.md

### 0.2.0

-   Added Docker support only with visual studio

### 0.1.0

-   Initial release
