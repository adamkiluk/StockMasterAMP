﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace StockMasterAMP.Server.Controllers
{
    public class HangfireController : ControllerBase
    {

        //in case to be secured that user is in premissions to view hangfire dashboard
        [Authorize]
        public IActionResult Index()
        {
            return Redirect("/hangfire");
        }
    }
}