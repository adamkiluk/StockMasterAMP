﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StockMasterAMP.Server.Managers;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.Dto.Bankier;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankierController : ControllerBase
    {
        private readonly IBankierManager _bankierManager;

        public BankierController(IBankierManager bankierManager)
        {
            _bankierManager = bankierManager;
        }

        //GET: api/Bankier
       [HttpGet("GetSymbols")]
       [AllowAnonymous]
        public async Task<ApiResponse> GetSymbols()
            => await _bankierManager.GetAllSymbols();

        //GET: api/Bankier
        [HttpGet("GetSubscriptions")]
        [AllowAnonymous]
        public async Task<ApiResponse> GetSubscriptions()
            => await _bankierManager.GetUserSubscriptions(this.User.Identity.Name);

        //POST: api/Bankier
        [HttpPost("Subscribe")]
        [AllowAnonymous]
        public async Task<ApiResponse> Subscribe([FromBody] Guid symbolId)
            => await _bankierManager.SubscribeToSymbol(symbolId, this.User.Identity.Name);

        //POST: api/Bankier
        [HttpPost("DeleteSubscription")]
        [AllowAnonymous]
        public async Task<ApiResponse> DeleteSubscription([FromBody] BankierSymbolsSubscriptionDto subscriptionToDelete)
            => await _bankierManager.DeleteUserSubscription(subscriptionToDelete);
        
    }
}
