﻿using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Namotion.Reflection;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels;
using StockMasterAMP.Shared.Dto.Bankier;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using static Microsoft.AspNetCore.Http.StatusCodes;
using static StockMasterAMP.Shared.AuthorizationDefinitions.Permissions;

namespace StockMasterAMP.Server.Managers
{
    public class BankierManager : IBankierManager
    {
        private readonly IBankierStore _bankierStore;
        private readonly UserManager<ApplicationUser> _userManager;

        public BankierManager(IBankierStore bankierStore, UserManager<ApplicationUser> userManager)
        {
            _bankierStore = bankierStore;
            _userManager = userManager;
        }

        public async Task<ApiResponse> GetSymbol(Guid id)
        {
            try
            {
                return new ApiResponse(Status200OK, "Retrieved Symbol by ID", await _bankierStore.GetSymbol(id));
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Retrieve Symbol by ID");
            }
        }

        public async Task<ApiResponse> GetSymbol(string name)
        {
            try
            {
                return new ApiResponse(Status200OK, "Retrieved Symbol by Name", await _bankierStore.GetSymbol(name));
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Retrieve Symbol by Name");
            }
        }

        public async Task<ApiResponse> GetAllSymbols()
        {
            try
            {
                return new ApiResponse(Status200OK, "Retrieved all Symbols", await _bankierStore.GetAllSymbols());
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Retrieve all Symbols");
            }
        }

        public async Task<ApiResponse> InsertSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            try
            {
                await _bankierStore.InsertSymbols(bankierSymbolsDto);
                return new ApiResponse(Status200OK, "Insert Symbols Sucessfully");
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Insert Symbols");
            }
        }

        public async Task<ApiResponse> UpdateSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            try
            {
                return new ApiResponse(Status200OK, "Symbols updated sucessfully", await _bankierStore.UpdateSymbols(bankierSymbolsDto));
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Update Symbols");
            }
        }

        public async Task<ApiResponse> DeleteSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            try
            {
                return new ApiResponse(Status200OK, "Deleted Symbols Sucessfully", await _bankierStore.DeleteSymbols(bankierSymbolsDto));
            }
            catch
            {
                return new ApiResponse(Status400BadRequest, "Failed to Delete Symbols");
            }
        }

        public async Task<ApiResponse> SubscribeToSymbol(Guid symboldId, string userName)
        {
            try
            {
                ApplicationUser user = _userManager.FindByNameAsync(userName).Result;

                if (user == null)
                {
                    new ApiResponse(Status400BadRequest, "Failed to Subscribe, user with no premissions or invalid");
                }

                var result = await _bankierStore.GetSymbol(symboldId);

                if (result == null)
                {
                    new ApiResponse(Status400BadRequest, "Failed to Subscribe");
                }

                BankierSymbolsSubscriptionDto subscriptionDto = new BankierSymbolsSubscriptionDto()
                {
                    Id = Guid.NewGuid(),
                    UserId = user.Id,
                    SymbolId = result.Id,
                };

                await _bankierStore.AddSymbol(subscriptionDto);

                return new ApiResponse(Status200OK, $"Subscribed Succesfully to {result.Name} on {result.StockType}");
            }
            catch (InvalidDataException dataException)
            {
                return new ApiResponse(Status400BadRequest, "Failed to Subscribe");
            }
        }

        public async Task<ApiResponse> GetUserSubscriptions(string userName)
        {
            try
            {
                ApplicationUser user = _userManager.FindByNameAsync(userName).Result;

                if (user == null)
                {
                    new ApiResponse(Status400BadRequest, "Failed to load subscriptions, user with no premissions or invalid");
                }

                List<BankierSymbolsSubscriptionDto> subscriptions = await _bankierStore.GetUserSubscriptions(user.Id);

                return new ApiResponse(Status200OK, "Subscriptions retrievieid succesfully", await _bankierStore.GetUserSubscriptions(user.Id));
            }
            catch (InvalidDataException dataException)
            {
                return new ApiResponse(Status400BadRequest, "Failed to Subscribe");
            }
        }

        public async Task<ApiResponse> DeleteUserSubscription(BankierSymbolsSubscriptionDto subscriptionToDelete)
        {
            try
            {
                await _bankierStore.DeleteUserSubscription(subscriptionToDelete.Id);

                return new ApiResponse(Status200OK, "Subscriptions deleted succesfully");
            }
            catch (InvalidDataException dataException)
            {
                return new ApiResponse(Status400BadRequest, "Failed to delete subscriptions");
            }
        }
    }
}
