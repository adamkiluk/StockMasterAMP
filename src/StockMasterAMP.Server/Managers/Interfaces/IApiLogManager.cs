﻿using System;
using System.Threading.Tasks;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.DataModels;

namespace StockMasterAMP.Server.Managers
{
    public interface IApiLogManager
    {
        Task Log(ApiLogItem apiLogItem);
        Task<ApiResponse> Get();
        Task<ApiResponse> GetByApplicationUserId(Guid applicationUserId);
    }
}