﻿using System.Threading.Tasks;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.Dto.Account;

namespace StockMasterAMP.Server.Managers
{
    public interface IUserProfileManager
    {
        Task<ApiResponse> Get();
        Task<ApiResponse> Upsert(UserProfileDto userProfile);
        Task<string> GetLastPageVisited(string userName);
    }
}