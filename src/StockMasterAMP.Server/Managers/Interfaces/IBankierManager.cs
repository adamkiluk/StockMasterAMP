﻿using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.Dto.Bankier;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Server.Managers
{
    public interface IBankierManager
    {
        //symbols
        Task<ApiResponse> GetSymbol(Guid id);
        Task<ApiResponse> GetSymbol(string name);
        Task<ApiResponse> GetAllSymbols();
        Task<ApiResponse> InsertSymbols(List<BankierSymbolDto> bankierSymbolsDto);
        Task<ApiResponse> UpdateSymbols(List<BankierSymbolDto> bankierSymbolsDto);
        Task<ApiResponse> DeleteSymbols(List<BankierSymbolDto> bankierSymbolsDto);

        //subs
        Task<ApiResponse> SubscribeToSymbol(Guid symbolId, string userName);
        Task<ApiResponse> GetUserSubscriptions(string userName);
        Task<ApiResponse> DeleteUserSubscription(BankierSymbolsSubscriptionDto subscriptionToDelete);
    }
}
