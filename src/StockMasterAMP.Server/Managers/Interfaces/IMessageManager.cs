﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.Dto;
using StockMasterAMP.Shared.Dto.Sample;

namespace StockMasterAMP.Server.Managers
{
    public interface IMessageManager
    {
        Task<ApiResponse> Create(MessageDto messageDto);
        List<MessageDto> GetList();
        Task<ApiResponse> Delete(int id);
    }
}