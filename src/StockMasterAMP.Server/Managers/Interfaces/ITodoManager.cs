﻿using System.Threading.Tasks;
using StockMasterAMP.Server.Middleware.Wrappers;
using StockMasterAMP.Shared.Dto.Sample;

namespace StockMasterAMP.Server.Managers
{
    public interface ITodoManager
    {
        Task<ApiResponse> Get();
        Task<ApiResponse> Get(long id);
        Task<ApiResponse> Create(TodoDto todo);
        Task<ApiResponse> Update(TodoDto todo);
        Task<ApiResponse> Delete(long id);
    }
}