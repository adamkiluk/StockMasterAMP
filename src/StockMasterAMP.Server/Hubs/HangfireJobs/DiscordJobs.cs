﻿using Discord;
using Discord.Webhook;
using StockMasterAMP.Discord;
using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.Dto.Bankier;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Server.Hubs.HangfireJobs
{
    public class DiscordJobs
    {
        private readonly IBankierStore _bankierStore;
        private readonly IDiscordWebhook _discordWebhook;

        public DiscordJobs(IBankierStore bankierStore, IDiscordWebhook discordWebhook)
        {
            _bankierStore = bankierStore;
            _discordWebhook = discordWebhook;
        }
        
        /// <summary>
        /// Compute existing database data, notificate all subscribers about their stock subscriptions
        /// algorithm : 
        /// -Get all subscriptions
        /// -Compute data and send discord messeages for users
        /// </summary>
        /// <returns> Task </returns>
        public async Task ProceedEvery5minsSubscriptionNotifications()
        {
            var bankierNews = await _bankierStore.GetAllBankierNewsForAllSubscribers();
            await _discordWebhook.SendMessage(bankierNews);

            foreach (var item in bankierNews)
            {
                item.DailyNotification = true;
            }

            await _bankierStore.SetBankierNewsAsAlredyReported(bankierNews);
        }
    }
}
