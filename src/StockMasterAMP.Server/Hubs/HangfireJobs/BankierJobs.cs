﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using StockMasterAMP.Shared.Dto.Bankier;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using StockMasterAMP.WebScraper.Bankier;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockMasterAMP.Server.Hubs.HangfireJobs
{
    public class BankierJobs
    {
        //check https://mehdi.me/ambient-dbcontext-in-ef6/
        private readonly IBankierStore _bankierStore;

        public BankierJobs(IBankierStore bankierStore)
        {
            _bankierStore = bankierStore;
        }

        public async Task ExecuteBankierLatestNewsEspiScrapper()
        {
            List<BankierSymbolDto> symbolsDto = await _bankierStore.GetAllSymbols();

            foreach (BankierSymbolDto symbol in symbolsDto)
            {
                 await CheckItemsAndSaveNew(symbol);
            }
        }

        public async Task ExecuteBankierSymbolLoader()
        {
            List<BankierSymbolDto> bankierSymbolsDto = BankierSymbolsLoader.GetSymbols();
            List<BankierSymbolDto> currentSymbolsDto = _bankierStore.GetAllSymbols().Result;

            if(currentSymbolsDto.Count != 0)
            {
                await CheckSymbolsToUpdate(bankierSymbolsDto);
            }
            else
            {
                await _bankierStore.InsertSymbols(bankierSymbolsDto);
            }
        }

        public async Task ExecuteBankiedLastedNewsEpsiScrapperForSubscriptions()
        {
            //get all users subscriptions
            List<BankierSymbolDto> symbolsDto = await _bankierStore.GetAllSymbolsForAllSubscribers();

            foreach (BankierSymbolDto symbol in symbolsDto)
            {
                await CheckItemsAndSaveNew(symbol);
            }
        }

        #region Daily scrapper for all data

        private async Task CheckItemsAndSaveNew(BankierSymbolDto symbol)
        {
            List<BankierNewsDto> news = BankierScraper.GetNews(symbol);
            List<BankierNewsDto> itemsToSave = new List<BankierNewsDto>();

            foreach (BankierNewsDto item in news)
            {
                bool exists = await _bankierStore.CheckIfBankierIdExistsInDatabase(item.BankierId);
                if (!exists)
                {
                    itemsToSave.Add(item);
                }
            }

            await ValidateAndSaveNewsOrEspi(symbol, itemsToSave);
        }

        private async Task ValidateAndSaveNewsOrEspi(BankierSymbolDto symbol, List<BankierNewsDto> newsOrEpsiList)
        {
            if (newsOrEpsiList.Count > 0)
            {
                foreach (BankierNewsDto item in newsOrEpsiList)
                {
                    item.SymbolId = symbol.Id;
                    item.Name = symbol.Name;
                }
                await _bankierStore.AddNews(newsOrEpsiList);
            }
        }

        private async Task CheckSymbolsToUpdate(List<BankierSymbolDto> bankierSymbolsDto)
        {
            List<BankierSymbolDto> currentSymbolsDto = _bankierStore.GetAllSymbols().Result;

            List<BankierSymbolDto> symbolsToRemove = currentSymbolsDto.Where(p => !bankierSymbolsDto.Any(l => p.Name == l.Name)).ToList();
            if (symbolsToRemove.Count > 0)
            {
                await _bankierStore.DeleteSymbols(symbolsToRemove);
            }

            List<BankierSymbolDto> symbolsToAdd = bankierSymbolsDto.Where(p => !currentSymbolsDto.Any(l => p.Name == l.Name)).ToList();
            if (symbolsToAdd.Count > 0)
            {
                 await _bankierStore.InsertSymbols(symbolsToRemove);
            }
        }
        #endregion
        #region Daily scrapper for subscribed stocks
        #endregion
    }
}
