﻿using System.Threading.Tasks;

namespace StockMasterAMP.Shared
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}