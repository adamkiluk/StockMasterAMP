﻿namespace StockMasterAMP.Shared.Dto.Enums.Bankier
{
    public enum BankierStockTypeDto
    {
        GPW = 0,
        NewConnect = 1,
    }
}
