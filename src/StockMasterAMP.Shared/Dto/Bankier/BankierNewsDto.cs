﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.Dto.Bankier
{
    public class BankierNewsDto : IAuditable, ISoftDelete
    {
        public Guid Id { get; set; }
        public Guid SymbolId { get; set; }
        public int BankierId { get; set; }
        public DateTime BankierDateTime { get; set; }
        public string Href { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public BankierNewsTypeDto NewsType { get; set; }
        public bool DailyNotification { get; set; }
        public bool WeeklyNotification { get; set; }
        public bool MonthlyNotification { get; set; }
    }
}
