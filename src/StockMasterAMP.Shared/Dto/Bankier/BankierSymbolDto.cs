﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.Dto.Bankier
{
    public class BankierSymbolDto : IAuditable, ISoftDelete
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }
        public string Title { get; set; }
        public BankierStockTypeDto StockType { get; set; }

    }
}
