﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels.Bankier;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.Dto.Bankier
{
    public class BankierSymbolsSubscriptionDto : IAuditable, ISoftDelete
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid SymbolId { get; set; }
    }
}
