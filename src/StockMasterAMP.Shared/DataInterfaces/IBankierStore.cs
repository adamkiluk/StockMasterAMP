﻿using StockMasterAMP.Shared.DataModels.Bankier;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using StockMasterAMP.Shared.Dto.Bankier;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Shared.DataInterfaces
{
    public interface IBankierStore
    {
        //SYMBOLS
        Task AddSymbol(BankierSymbolsSubscriptionDto symbolDto);
        Task<BankierSymbolDto> GetSymbol(Guid id);
        Task<BankierSymbolDto> GetSymbol(string name);
        Task<List<BankierSymbolDto>> GetUserSymbolsBySubscriptions(List<BankierSymbolsSubscriptionDto> subscriptionsDto);
        Task<List<BankierSymbolDto>> GetAllSymbols();
        Task InsertSymbols(List<BankierSymbolDto> bankierSymbolsDto);
        Task<List<BankierSymbol>> UpdateSymbols(List<BankierSymbolDto> bankierSymbolsDto);
        Task<List<BankierSymbol>> DeleteSymbols(List<BankierSymbolDto> bankierSymbolsDto);
        Task<List<BankierSymbolDto>> GetAllSymbolsForAllSubscribers();

        //SUBS
        Task <List<BankierSymbolsSubscriptionDto>> GetUserSubscriptions(Guid userId);
        Task DeleteUserSubscription(Guid userSubscriptionId);

        //NEWS
        Task SetBankierNewsAsAlredyReported(List<BankierNewsDto> bankierNews);
        Task<List<BankierNewsDto>> GetAllBankierNewsForAllSubscribers();
        Task<List<BankierNewsDto>> LoadLastFiveNewsOrEspiSymbol(Guid symbolId, BankierNewsType newsType);
        Task AddNews(List<BankierNewsDto> newsDto);
        Task<bool> CheckIfBankierIdExistsInDatabase(int id);
    }
}
