﻿using StockMasterAMP.Server.Models;
using StockMasterAMP.Shared.Dto.Tenant;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StockMasterAMP.Shared.DataInterfaces
{
    public interface ITenantStore
    {
        List<TenantDto> GetAll();

        TenantDto GetById(long id);

        Task<Tenant> Create(TenantDto tenantDto);

        Task<Tenant> Update(TenantDto tenantDto);

        Task DeleteById(long id);
    }
}
