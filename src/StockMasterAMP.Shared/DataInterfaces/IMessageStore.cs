﻿using System.Collections.Generic;
using System.Threading.Tasks;
using StockMasterAMP.Shared.DataModels;
using StockMasterAMP.Shared.Dto.Sample;

namespace StockMasterAMP.Shared.DataInterfaces
{
    public interface IMessageStore
    {
        Task<Message> AddMessage(MessageDto messageDto);

        Task DeleteById(int id);

        List<MessageDto> GetMessages();
    }
}