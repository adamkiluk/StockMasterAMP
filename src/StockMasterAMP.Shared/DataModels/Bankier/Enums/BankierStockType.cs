﻿namespace StockMasterAMP.Shared.DataModels.Bankier.Enums
{
    public enum BankierStockType
    {
        GPW = 0,
        NewConnect = 1,
    }
}
