﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.DataModels.Bankier
{
    public class BankierNews : IAuditable, ISoftDelete
    {
        [Key]
        public Guid Id { get; set; }
        public Guid SymbolId { get; set; }
        public int BankierId { get; set; }
        public DateTime BankierDateTime { get; set; }
        public string Href { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public BankierNewsType NewsType { get; set; }
        public bool DailyNotification { get; set; }
        public bool WeeklyNotification { get; set; }
        public bool MonthlyNotification { get; set; }
    }
}
