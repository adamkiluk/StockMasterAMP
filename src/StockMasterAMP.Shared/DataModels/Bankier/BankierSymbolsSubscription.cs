﻿using StockMasterAMP.Shared.DataInterfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.DataModels.Bankier
{
    public class BankierSymbolsSubscription : IAuditable, ISoftDelete
    {
        [Key]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid SymbolId { get; set; }
    }
}
