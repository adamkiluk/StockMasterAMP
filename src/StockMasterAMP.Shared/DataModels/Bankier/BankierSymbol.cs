﻿using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockMasterAMP.Shared.DataModels.Bankier
{
    public class BankierSymbol : IAuditable, ISoftDelete
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Href { get; set; }
        public string Title { get; set; }
        public BankierStockType StockType { get; set; }
    }
}
