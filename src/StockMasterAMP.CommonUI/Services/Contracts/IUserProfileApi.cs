﻿using System.Threading.Tasks;
using StockMasterAMP.Shared.Dto;
using StockMasterAMP.Shared.Dto.Account;

namespace StockMasterAMP.CommonUI.Services.Contracts
{
    /// <summary>
    /// Access to User Profile information
    /// </summary>
    public interface IUserProfileApi
    {
        Task<ApiResponseDto> Upsert(UserProfileDto userProfile);
        Task<ApiResponseDto> Get();
    }
}
