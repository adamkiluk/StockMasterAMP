FROM mcr.microsoft.com/dotnet/core/aspnet:3.1.3-buster-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1.201-buster AS build
WORKDIR /src
COPY ["StockMasterAMP.Server/StockMasterAMP.Server.csproj", "StockMasterAMP.Server/"]
COPY ["StockMasterAMP.Shared/StockMasterAMP.Shared.csproj", "StockMasterAMP.Shared/"]
COPY ["StockMasterAMP.Client/StockMasterAMP.Client.csproj", "StockMasterAMP.Client/"]
COPY ["StockMasterAMP.CommonUI/StockMasterAMP.CommonUI.csproj", "StockMasterAMP.CommonUI/"]
COPY ["StockMasterAMP.Storage/StockMasterAMP.Storage.csproj", "StockMasterAMP.Storage/"]
RUN dotnet restore "StockMasterAMP.Server/StockMasterAMP.Server.csproj" --disable-parallel 
COPY . .
WORKDIR "/src/StockMasterAMP.Server"
ARG BUILD_CONFIG="Release_CSB"
RUN dotnet build "StockMasterAMP.Server.csproj" -c $BUILD_CONFIG -o /app/build --no-restore

FROM build AS publish
ARG BUILD_CONFIG="Release_CSB"
RUN dotnet publish "StockMasterAMP.Server.csproj" -c $BUILD_CONFIG -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "StockMasterAMP.Server.dll"]