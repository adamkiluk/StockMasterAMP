﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels.Bankier;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using StockMasterAMP.Shared.Dto.Bankier;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StockMasterAMP.Storage.Stores
{
    public class BankierStore : IBankierStore
    {
        private readonly IApplicationDbContext _db;
        private readonly IMapper _autoMapper;

        public BankierStore(IApplicationDbContext db, IMapper autoMapper)
        {
            _db = db;
            _autoMapper = autoMapper;
        }

        public async Task<BankierSymbolDto> GetSymbol(Guid id)
        {
            var bankierSymbol = await _db.BankierSymbols.SingleOrDefaultAsync(t => t.Id == id);

            if (bankierSymbol == null)
                throw new InvalidDataException($"Unable to find BankierSymbol with ID: {id}");

            return _autoMapper.Map<BankierSymbolDto>(bankierSymbol);
        }

        public async Task<BankierSymbolDto> GetSymbol(string name)
        {
            var bankierSymbol = await _db.BankierSymbols.SingleOrDefaultAsync(t => t.Name == name);

            if (bankierSymbol == null)
                throw new InvalidDataException($"Unable to find BankierSymbol with Name: {name}");

            return _autoMapper.Map<BankierSymbolDto>(bankierSymbol);
        }

        public async Task<List<BankierSymbolDto>> GetAllSymbols()
        {
            return await _autoMapper.ProjectTo<BankierSymbolDto>(_db.BankierSymbols).ToListAsync();
        }

        public async Task<List<BankierSymbolDto>> GetUserSymbolsBySubscriptions(List<BankierSymbolsSubscriptionDto> subscriptions)
        {
            List<BankierSymbol> symbols = new List<BankierSymbol>();

            foreach(BankierSymbolsSubscriptionDto subscription in subscriptions)
            {
                BankierSymbol symbol = await _db.BankierSymbols.Where(x => x.Id == subscription.SymbolId).FirstOrDefaultAsync();
                symbols.Add(symbol);
            }
            var symboldDto = _autoMapper.Map<List<BankierSymbol>, List<BankierSymbolDto>>(symbols);
            return symboldDto;
        }
        public async Task InsertSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            List<BankierSymbol> bankierSymbols = _autoMapper.Map<List<BankierSymbolDto>, List<BankierSymbol>>(bankierSymbolsDto);
            await _db.BankierSymbols.AddRangeAsync(bankierSymbols);
            await _db.SaveChangesAsync(CancellationToken.None);
        }

        public async Task<List<BankierSymbol>> UpdateSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            List<BankierSymbol> returnSymbols = new List<BankierSymbol>();

            foreach (BankierSymbolDto bankierSymbolDto in bankierSymbolsDto)
            {
                var bankierSymbol = await _db.BankierSymbols.SingleOrDefaultAsync(t => t.Id == bankierSymbolDto.Id);
                if (bankierSymbol == null)
                    throw new InvalidDataException($"Unable to find BankierSymbol with ID: {bankierSymbolDto.Id}");

                returnSymbols.Add(_autoMapper.Map(bankierSymbolDto, bankierSymbol));
                _db.BankierSymbols.Update(bankierSymbol);
            }

            await _db.SaveChangesAsync(CancellationToken.None);

            return returnSymbols;
        }

        public async Task<List<BankierSymbol>> DeleteSymbols(List<BankierSymbolDto> bankierSymbolsDto)
        {
            var bankierSymbols = _autoMapper.Map<List<BankierSymbolDto>, List<BankierSymbol>>(bankierSymbolsDto);
            _db.BankierSymbols.RemoveRange(bankierSymbols);
             await _db.SaveChangesAsync(CancellationToken.None);
            return bankierSymbols;
        }

        public async Task AddSymbol(BankierSymbolsSubscriptionDto symbolDto)
        {
            BankierSymbolsSubscription bankierSymbolSubscription =
                _autoMapper.Map<BankierSymbolsSubscriptionDto, BankierSymbolsSubscription>(symbolDto);

            await _db.BankierSymbolsSubscriptions.AddAsync(bankierSymbolSubscription);
            await _db.SaveChangesAsync(CancellationToken.None);
        }

        public async Task<List<BankierSymbolDto>> GetAllSymbolsForAllSubscribers()
        {
            List<BankierSymbolsSubscription> userSubscriptions = await _db.BankierSymbolsSubscriptions.Distinct().ToListAsync();
            //List<BankierSymbol> symbols = await _db.BankierSymbols.ToListAsync();

            IQueryable<BankierSymbol> symbols = from symbol in _db.BankierSymbols
                                                join sub in _db.BankierSymbolsSubscriptions on symbol.Id equals sub.SymbolId
                                                select symbol;

            return _autoMapper.Map<List<BankierSymbol>, List<BankierSymbolDto>>(symbols.ToList());
        }

        public async Task<List<BankierSymbolsSubscriptionDto>> GetUserSubscriptions(Guid userId)
        {
            List<BankierSymbolsSubscription> userSubscriptions = await _db.BankierSymbolsSubscriptions.Where(x => x.UserId == userId).ToListAsync();
            return _autoMapper.Map<List<BankierSymbolsSubscription>, List<BankierSymbolsSubscriptionDto>>(userSubscriptions);
        }

        public async Task DeleteUserSubscription(Guid userSubscriptionId)
        {
            BankierSymbolsSubscription userSubscription = await _db.BankierSymbolsSubscriptions.Where(x => x.Id == userSubscriptionId).FirstAsync();
            _db.BankierSymbolsSubscriptions.Remove(userSubscription);
            await _db.SaveChangesAsync(CancellationToken.None);
        }

        public async Task SetBankierNewsAsAlredyReported(List<BankierNewsDto> bankierNewsDto)
        {
            var bankierNews = _autoMapper.Map<List<BankierNewsDto>, List<BankierNews>>(bankierNewsDto);
            _db.BankierNews.UpdateRange(bankierNews);

            await _db.SaveChangesAsync(CancellationToken.None);
        }

        public async Task<List<BankierNewsDto>> LoadLastFiveNewsOrEspiSymbol(Guid symbolId, BankierNewsType newsType)
        {
            List<BankierNews> dbNews = await _db.BankierNews
                .Where(x => x.SymbolId == symbolId && x.NewsType == newsType)
                .OrderByDescending(y => y.BankierDateTime)
                .Take(5)
                .ToListAsync();

            List<BankierNewsDto> bankierNewsDto = _autoMapper.Map<List<BankierNews>, List<BankierNewsDto>>(dbNews);
            return bankierNewsDto;
        }

        public async Task<bool> CheckIfBankierIdExistsInDatabase(int id)
         => await _db.BankierNews.Where(x => x.BankierId == id).CountAsync() > 0;
        

        public async Task AddNews(List<BankierNewsDto> newsDto)
        {
            List<BankierNews> bankierNews = _autoMapper.Map<List<BankierNewsDto>, List<BankierNews>>(newsDto);
            await _db.BankierNews.AddRangeAsync(bankierNews);
            await _db.SaveChangesAsync(CancellationToken.None);
        }

        public async Task<List<BankierNewsDto>> GetAllBankierNewsForAllSubscribers()
        {
            List<Guid> subcriptionGuids =  _db.BankierSymbolsSubscriptions.AsNoTracking().Select(item => item.SymbolId).Distinct().ToList();

            List<BankierNews> bankierNews = new List<BankierNews>();
            foreach (Guid subscription in subcriptionGuids)
            {
                bankierNews.AddRange(_db.BankierNews.AsNoTracking().Where(item => item.SymbolId == subscription && item.DailyNotification != true).ToList());
            }

            List<BankierNewsDto> bankierNewsDto = new List<BankierNewsDto>();
            bankierNewsDto = await Task.Run(() => _autoMapper.Map<List<BankierNews>, List<BankierNewsDto>>(bankierNews));

            return bankierNewsDto;
        }
    }
}
