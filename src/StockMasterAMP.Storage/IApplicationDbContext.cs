﻿using StockMasterAMP.Server.Models;
using StockMasterAMP.Shared.DataInterfaces;
using StockMasterAMP.Shared.DataModels;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using StockMasterAMP.Shared.DataModels.Bankier;

namespace StockMasterAMP.Storage
{
    public interface IApplicationDbContext
    {
        public DbSet<ApiLogItem> ApiLogs { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Todo> Todos { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<ApplicationUser> Users { get; set; }
        //public DbSet<Tenant> Tenants { get; set; }

        //Bankier
        public DbSet<BankierSymbol> BankierSymbols { get; set; }
        public DbSet<BankierSymbolsSubscription> BankierSymbolsSubscriptions { get; set; }
        public DbSet<BankierNews> BankierNews { get; set; }

        public void SetGlobalQueryForSoftDelete<T>(ModelBuilder builder) where T : class, ISoftDelete;

        public void SetGlobalQueryForSoftDeleteAndTenant<T>(ModelBuilder builder) where T : class, ISoftDelete, ITenant;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        int SaveChanges();

    }
}
