﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StockMasterAMP.Storage.Migrations
{
    public partial class BankierNewsTableUpdate_01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "BankierNews",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "BankierNews");
        }
    }
}
