﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace StockMasterAMP.Storage.Migrations
{
    public partial class NotificationDbSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "DailyNotification",
                table: "BankierNews",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "MonthlyNotification",
                table: "BankierNews",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "WeeklyNotification",
                table: "BankierNews",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DailyNotification",
                table: "BankierNews");

            migrationBuilder.DropColumn(
                name: "MonthlyNotification",
                table: "BankierNews");

            migrationBuilder.DropColumn(
                name: "WeeklyNotification",
                table: "BankierNews");
        }
    }
}
