﻿using AutoMapper;
using AutoMapper.Configuration;
using StockMasterAMP.Shared.DataModels;
using StockMasterAMP.Shared.DataModels.Bankier;
using StockMasterAMP.Shared.DataModels.Bankier.Enums;
using StockMasterAMP.Shared.Dto;
using StockMasterAMP.Shared.Dto.Account;
using StockMasterAMP.Shared.Dto.Bankier;
using StockMasterAMP.Shared.Dto.Enums.Bankier;
using StockMasterAMP.Shared.Dto.Sample;
using System.Collections.Generic;
using ApiLogItem = StockMasterAMP.Shared.DataModels.ApiLogItem;
using Message = StockMasterAMP.Shared.DataModels.Message;
using UserProfile = StockMasterAMP.Shared.DataModels.UserProfile;

namespace StockMasterAMP.Storage.Mapping
{
    public class MappingProfile : MapperConfigurationExpression
    {
        /// <summary>
        /// Create automap mapping profiles
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Todo, TodoDto>().ReverseMap();           
            CreateMap<UserProfile, UserProfileDto>().ReverseMap();
            CreateMap<ApiLogItem, ApiLogItemDto>().ReverseMap();
            CreateMap<Message, MessageDto>().ReverseMap();

            //Bankier
            CreateMap<BankierSymbol, BankierSymbolDto>().ReverseMap();
            CreateMap<BankierStockType, BankierStockTypeDto>().ReverseMap();
            CreateMap<BankierStockTypeDto, BankierStockType>(MemberList.Destination);
            CreateMap<BankierStockTypeDto, BankierStockType>(MemberList.Source);

            CreateMap<BankierSymbol, BankierSymbolDto>()
                .ForMember(m => m.StockType, opt => opt.MapFrom(x => (BankierStockTypeDto)(int)x.StockType));

            CreateMap<BankierSymbolsSubscription, BankierSymbolsSubscriptionDto>().ReverseMap();

            CreateMap<BankierNews, BankierNewsDto>().ReverseMap();
            CreateMap<BankierNews, BankierNewsDto>()
                .ForMember(m => m.NewsType, opt => opt.MapFrom(x => (BankierNewsTypeDto)(int)x.NewsType));
        }
    }
}
